import Basic from "components/ddx/Basic.tmpl_20AD45E7.jsx";
import Sample from "components/ddx/Samples.tmpl_636E68FA.jsx";
import Image from "components/ddx/Image.tmpl_1AC73C98.jsx";
import Person from "components/ddx/Person.tmpl_B6E9AF9B.jsx";

export { Basic, Image, Sample, Person };
